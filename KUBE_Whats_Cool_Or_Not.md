# Cool or not ?

A kind of beast vs a single podman/docker machine

## The Good

* state management
* recreate on other nodes
* extensible (create your own operators)
* namespaces isolation
* stateless pods
* statefull pod is an add-on introduced later on but is ok now
* ingress
* helm ( ~ docker-compose)
* high availability with master election (but more complex)
* 1 IP per pod (and service, ...) ie no port conflict/hanling for multiple apps running on same host
* auto service discovery/load-balancing

## The Bad

* many distribs, choice but different management for prod
* components can be changed, but behavior can be different
  * usage of metadata, so will be different for an other component, recipe may not work well on an other kube distrib
* use this instead of that (though still exists and does "the same")

## The Ugly

* according to distrib battery included, or not...
* lots of extensions, alpha then beta then prod or disappear... need to adapt, keep up-to-date
* certs management (rotation), mostly auto but more or less...
* RBAC (roles, authorization, users): complex
* network handling
* lots of components to learn/understand (pod, service, deployment, ingress, cronjob, batchjob, daemonsets, replicasets, statefulsets, etc.)
* mutiple virtual networks (inter and intra node) with
  possibly multiple tools, hard to debug

## good and bad

* no patch and restart, either patch is live, or need to recreate pod
* CSI standard for external storage allocation but drivers support life cycle (may not be supported for new/old storages, not same features everywhere etc...)
