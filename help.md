# with minikube

## start minikube

minikube start --driver=docker
minikube addons enable ingress
minikube addons enable ingress-dns


## step by step

    osallou@ptb-03220125 (master) $ kubie ctx minikube
    [minikube|default] osallou@ptb-03220125 resources (master) $ kubectl create ns demo
    [minikube|default] osallou@ptb-03220125 resources (master) $ kubie ns tuto1
    [minikube|demo] osallou@ptb-03220125 resources (master) $ kubectl apply -f pod.yaml
    [minikube|demo] osallou@ptb-03220125 resources (master) $ kubectl get pods -o wide
    [minikube|demo] osallou@ptb-03220125 resources (master) $ kubectl apply -f service.yaml
    service/foo-service created
    [minikube|demo] osallou@ptb-03220125 resources (master) $ kubectl get svc -o wide
    NAME          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE   SELECTOR
    foo-service   ClusterIP   10.108.219.207   <none>        8080/TCP   10s   app=foo
    [minikube|demo] osallou@ptb-03220125 resources (master) $ kubectl apply -f ingress.yaml
    ingress.networking.k8s.io/demo-ingress created
    [minikube|demo] osallou@ptb-03220125 resources (master) $ kubectl get ingress
    NAME           CLASS   HOSTS   ADDRESS        PORTS   AGE
    demo-ingress   nginx   *       192.168.49.2   80      2m1s
    [minikube|demo] osallou@ptb-03220125 resources (master) $ curl http://192.168.49.2/demo

## display more info

The -o wide gives more info than default in CLI

    $ kubectl get nodes -o wide
    NAME       STATUS   ROLES           AGE     VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION           CONTAINER-RUNTIME
    minikube   Ready    control-plane   4d18h   v1.26.3   192.168.49.2   <none>        Ubuntu 20.04.5 LTS   6.2.13-300.fc38.x86_64   docker://23.0.2

## access pods network / debug

Run a temporary interactive job, in expected namespace

    $ kubectl run -i --tty --rm debug --image=debian --restart=Never -- /bin/bash
    $ docker exec -it minikube /bin/bash
    ...
    # in container
    ping 10.244.0.13 (ip a of pod)

Other usefull debug images:

* https://github.com/nicolaka/netshoot
* https://github.com/lightrun-platform/koolkits#readme

## To access ingress port

    $ kubectl get ingress
    NAME              CLASS   HOSTS   ADDRESS        PORTS   AGE
    example-ingress   nginx   *       192.168.49.2   80      17h

    curl http://192.168.49.2/foo

## helm

https://helm.sh/docs/intro/install/

Create a template of chart if needed and update files

    $ kubectl create ns demohelm
    $ kubie ns demohelm
    [minikube|demohelm] osallou@ptb-03220125 tuto (master) $ helm create tuto
    # update helm...
    # to get files with no exec/apply use --dry-run
    [minikube|demohelm] osallou@ptb-03220125 tuto (master) $ helm install -n demohelm tuto .
    # default template expose service via clusterip not ingress
    [minikube|demohelm] osallou@ptb-03220125 tuto (master) $ kubectl describe svc tuto
    Name:              tuto
    Namespace:         demohelm
    Labels:            app.kubernetes.io/instance=tuto
                    app.kubernetes.io/managed-by=Helm
                    app.kubernetes.io/name=tuto
                    app.kubernetes.io/version=1.16.0
                    helm.sh/chart=tuto-0.1.0
    Annotations:       meta.helm.sh/release-name: tuto
                    meta.helm.sh/release-namespace: demohelm
    Selector:          app.kubernetes.io/instance=tuto,app.kubernetes.io/name=tuto
    Type:              ClusterIP
    IP Family Policy:  SingleStack
    IP Families:       IPv4
    IP:                10.109.46.89
    IPs:               10.109.46.89
    Port:              http  80/TCP
    TargetPort:        http/TCP
    Endpoints:         10.244.0.21:80
    Session Affinity:  None
    Events:            <none>

    [minikube|demohelm] osallou@ptb-03220125 tuto (master) $ kubectl run --namespace demohelm -i --tty --rm debug --image=nicolaka/netshoot --restart=Never -- /bin/bash
    $ curl http://10.109.46.89/test.html

To uninstall:

    helm uninstall tuto